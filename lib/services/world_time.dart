import 'package:http/http.dart';
import 'package:intl/intl.dart';
import 'dart:convert';

class WorldTime {
  String location; // locations for UI
  String time = 'nothing'; // time in diffrent locations
  String flag; // flag icons for each locations
  String url; // locations url for api
  bool isDaytime = true; // this is for checking if time is day or night?!

  WorldTime({required this.location, required this.flag, required this.url});
  Future<void> gettime() async {
    try {
      Response response =
          await get(Uri.parse('http://worldtimeapi.org/api/timezone/$url'));
      Map data = jsonDecode(response.body);

      // string datetime
      String datetime = data['datetime'];
      String offset = data['utc_offset'].substring(1, 3);

      DateTime now = DateTime.parse(datetime);
      now = now.add(Duration(hours: int.parse(offset)));

      // check for day light
      isDaytime = now.hour > 6 && now.hour < 19 ? true : false;

      // get time and make it to readable format by package intl
      time = DateFormat.jm().format(now);
    } catch (e) {
      time = "Could'n get time :(";
    }
  }
}
