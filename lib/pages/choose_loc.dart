import 'package:flutter/material.dart';
import 'package:flag/flag.dart';
import 'package:world_time/services/world_time.dart';

class ChooseLoc extends StatefulWidget {
  const ChooseLoc({Key? key}) : super(key: key);

  @override
  _ChooseLocState createState() => _ChooseLocState();
}

class _ChooseLocState extends State<ChooseLoc> {
  List<WorldTime> locations = [
    WorldTime(url: 'Europe/London', location: 'London', flag: 'gb'),
    WorldTime(url: 'Europe/Berlin', location: 'Athens', flag: 'gb'),
    WorldTime(url: 'Iran/Tehran', location: 'Tehran', flag: 'ir'),
    WorldTime(url: 'Africa/Cairo', location: 'Cairo', flag: 'eg'),
    WorldTime(url: 'Africa/Nairobi', location: 'Nairobi', flag: 'ke'),
    WorldTime(url: 'America/Chicago', location: 'Chicago', flag: 'us'),
    WorldTime(url: 'America/New_York', location: 'New York', flag: 'us'),
    WorldTime(url: 'Asia/Jakarta', location: 'Jakarta', flag: 'id'),
  ];

  void updater(index) async {
    WorldTime instance = locations[index];
    await instance.gettime();
    // go to home
    Navigator.pop(context, {
      'location': instance.location,
      'flag': instance.flag,
      'time': instance.time,
      'isDaytime': instance.isDaytime,
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color(0xff236B72),
        title: const Text('choose location'),
        centerTitle: true,
      ),
      body: ListView.builder(
        itemCount: locations.length,
        itemBuilder: (context, index) {
          return Padding(
            padding: const EdgeInsets.fromLTRB(2, 5, 2, 2),
            child: Card(
              child: ListTile(
                onTap: () {
                  updater(index);
                },
                title: Text(locations[index].location),
                leading: Flag.fromString(
                  locations[index].flag,
                  width: 90,
                  height: 20,
                ),
                hoverColor: const Color(0xff368f8a),
                subtitle: Text(locations[index].url),
              ),
            ),
          );
        },
      ),
    );
  }
}
