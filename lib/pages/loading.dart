import 'package:flutter/material.dart';
import 'package:world_time/services/world_time.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class Loading extends StatefulWidget {
  const Loading({Key? key}) : super(key: key);

  @override
  _LoadingState createState() => _LoadingState();
}

class _LoadingState extends State<Loading> {
  void setupworlftime() async {
    WorldTime instance =
        WorldTime(location: 'Tehran', flag: 'germany.png', url: 'Asia/Tehran');
    await instance.gettime();
    Navigator.pushReplacementNamed(context, '/home', arguments: {
      'location': instance.location,
      'flag': instance.flag,
      'time': instance.time,
      'isDaytime': instance.isDaytime,
    });
  }

  @override
  void initState() {
    super.initState();
    setupworlftime();
  }

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      backgroundColor: Color(0xff236B72),
      body: Center(
        child: SpinKitDoubleBounce(
          color: Color(0xffF5F2E7),
          size: 50.0,
        ),
      ),
    );
  }
}
