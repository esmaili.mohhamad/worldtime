import 'package:flutter/material.dart';
import 'package:world_time/pages/choose_loc.dart';
import 'package:world_time/pages/home.dart';
import 'package:world_time/pages/loading.dart';

void main() {
  runApp(
    MaterialApp(
      // initialRoute: '/home',
      debugShowCheckedModeBanner: false,
      routes: {
        '/': (context) => const Loading(),
        '/home': (context) => const Home(),
        '/location': (context) => const ChooseLoc(),
      },
    ),
  );
}
